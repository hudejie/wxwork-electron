# vue3-eletron-demo001

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run start:vue
npm run start:exe
```

### Compiles and minifies for production
```
npm run build:vue
npm run build:exe
```

### 界面展示
![界面展示](screenshot/1.png)
![界面展示](screenshot/2.png)
![界面展示](screenshot/3.png)
![界面展示](screenshot/4.png)
![界面展示](screenshot/5.png)
![界面展示](screenshot/6.png)
![界面展示](screenshot/7.png)
![界面展示](screenshot/8.png)
![界面展示](screenshot/9.png)
