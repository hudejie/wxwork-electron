const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})


'use strict'
const path = require('path');

function resolve(dir) {
    return path.join(__dirname, dir);
}

module.exports = {
    publicPath: './',
    pluginOptions: {
        electronBuilder: {
            externals: ["electron-screenshots"],
        }
    },
    pages: {
        index: {
            entry: 'src/vue/main.js',
            template: 'src/vue/public/index.html',
        }
    },
    outputDir: 'src/vue/renderer',
    productionSourceMap: false,
    chainWebpack: (config) => {
        config.resolve.alias
            .set('@', resolve('src/vue'))
    }
}