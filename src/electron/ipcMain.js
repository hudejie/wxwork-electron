const { app, ipcMain, BrowserWindow } = require('electron');
const { startCapture, endCapture } = require("./screenshoots");
const { getDiskInfo } = require("./diskinfo");

const handle = (name, callback) => {
    ipcMain.handle(name, (e, ...par) => callback(BrowserWindow.fromWebContents(e.sender), e, ...par))
}

// 打开调试
handle("toggle_dev_tools", (win) => win.webContents.toggleDevTools())

// 重启
handle("restart", () => {
    app.relaunch();
    app.exit(0)
})

handle("close", () => {
    app.exit(0)
})

// 最小化
handle("min", (win) => win.minimize())

// 最大化
handle("max", (win) => {
    if (win.isMaximized()) {
        win.unmaximize()
    } else {
        win.maximize()
    }
})

handle("getDiskInfo", () => {return getDiskInfo()} )

handle("startCapture", () => startCapture())
handle("endCapture", () => endCapture())
