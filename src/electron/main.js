// main.js

// Modules to control application life and create native browser window
const { app, BrowserWindow } = require('electron')
const path = require('node:path')
const { initScreenshoots } = require("./screenshoots");

global.appDirname = __dirname;

app.commandLine.appendSwitch('disable-features', 'OutOfBlinkCors');

let winURL = path.resolve(__dirname, "../vue/renderer/index.html");

const createWindow = () => {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 986,
    height: 650,
    minWidth: 910,
    minHeight: 650,
    resizable: true,
    useContentSize: true,
    frame: false,
    titleBarStyle: "hidden",
    // transparent: true,//透明窗口
    backgroundColor: '#f1f1f1',//窗口底色为透明色
    show: false,
    icon: path.join(__dirname,"./WXWork_00001.ico"),
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      webSecurity: false,
      nodeIntegration: true,
      webviewTag: true
    }
  })

  if (app.isPackaged)
  {
    mainWindow.loadURL(`file://${winURL}`)
  }
  else
  {
    mainWindow.loadURL('http://localhost:8080/')
    //mainWindow.webContents.openDevTools()
  }

  mainWindow.on('ready-to-show', () => { mainWindow.show() })
}

// 这段程序将会在 Electron 结束初始化
// 和创建浏览器窗口的时候调用
// 部分 API 在 ready 事件触发后才能使用。
app.whenReady().then(() => {
    initScreenshoots();
    require("./ipcMain")
    createWindow()

  app.on('activate', () => {
    // 在 macOS 系统内, 如果没有已开启的应用窗口
    // 点击托盘图标时通常会重新创建一个新窗口
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// 除了 macOS 外，当所有窗口都被关闭的时候退出程序。 因此, 通常
// 对应用程序和它们的菜单栏来说应该时刻保持激活状态, 
// 直到用户使用 Cmd + Q 明确退出
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
})

// 在当前文件中你可以引入所有的主进程代码
// 也可以拆分成几个文件，然后用 require 导入。