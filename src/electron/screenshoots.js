const { globalShortcut } = require("electron");
const Screenshots = require("electron-screenshots");

let screenshots = null;

module.exports.initScreenshoots = () => {
    console.log(Screenshots);
    screenshots = new Screenshots();
    globalShortcut.register("ctrl+q", () => {
        screenshots.startCapture();
    });
    // 点击确定按钮回调事件
    screenshots.on("ok", (e, buffer, bounds) => {
        console.log("capture", buffer, bounds);
    });
    // 点击取消按钮回调事件
    screenshots.on("cancel", () => {
        console.log("capture", "cancel1");
        screenshots.endCapture();
    });
    screenshots.on("cancel", (e) => {
        // 执行了preventDefault
        // 点击取消不会关闭截图窗口
        e.preventDefault();
        console.log("capture", "cancel2");
        screenshots.endCapture();
    });
    // 点击保存按钮回调事件
    screenshots.on("save", (e, buffer, bounds) => {
        console.log("capture", buffer, bounds);
    });
    // esc取消
    globalShortcut.register("esc", () => {
        if (screenshots.$win?.isFocused()) {
            screenshots.endCapture();
        }
    });
};

module.exports.startCapture = () => {
    screenshots.startCapture();
}

module.exports.endCapture = () => {
    screenshots.endCapture();
}
