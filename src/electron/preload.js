const { contextBridge, ipcRenderer } = require('electron')

// 消息推送
const invokesApi = {}
const invokes = [
    "min",
    "max",
    "close",
    "toggle_dev_tools",
    "startCapture",
    "endCapture",
    "getDiskInfo"
]
invokes.forEach(item => {
    invokesApi[item] = (...res) => ipcRenderer.invoke(item, ...res)
})

contextBridge.exposeInMainWorld('send', invokesApi)

console.log(invokesApi)
