import en from "./en.js"
import zh from "./zh.js"

import enLocale from 'element-plus/dist/locale/zh-cn.mjs'
import zhLocale from 'element-plus/dist/locale/en.mjs'

//语言
import { createI18n } from 'vue-i18n'		//引入vue-i18n组件


const messages = {
    [enLocale.name]: {
        // el 这个属性很关键，一定要保证有这个属性，
        el: enLocale.el,
        // 定义你自己的字典，但是请不要和 `el` 重复，这样会导致 ElementPlus 内部组件的翻译失效.
        message: en,
    },
    [zhLocale.name]: {
        el: zhLocale.el,
        // 定义你自己的字典，但是请不要和 `el` 重复，这样会导致 ElementPlus 内部组件的翻译失效.
        message: zh,
    },
    testLocale: {
        el: {},
        // 没有定义 message 字段，会 fallback 回到 en 去, fallbackLocale 的定义在下方 👇
    },
}
console.log(messages);

const i18n = createI18n({
    locale: zhLocale.name,
    fallbackLocale: enLocale.name,
    messages,
})

export default i18n; //将i18n暴露出去，在main.js中引入挂载

