import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'                               // 引入路由配置
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import language from './language';

const app = createApp(App)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

const debounce = (fn, delay) => {
    let timer = null;
    return function () {
      let context = this;
      let args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        fn.apply(context, args);
      }, delay);
    }
}

const _ResizeObserver = window.ResizeObserver;
window.ResizeObserver = class ResizeObserver extends _ResizeObserver{
    constructor(callback) {
        callback = debounce(callback, 16);
        super(callback);
    }
}
  
// 正式环境清除所有console.log
if (process.env.NODE_ENV === 'production') {
    if (window) {
        window.console.log = function () {};
    }
}

app.use(ElementPlus)
app.use(router)
app.use(language)
app.mount('#app')
