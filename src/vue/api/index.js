import request from '../utils/request';

export const fetchData = query => {
    return request({
        url: './table.json',
        method: 'get',
        params: query
    });
};

export const fetchIconFontData = () => {
    return request({
        url: './iconfont.json',
        method: 'get',
    });
};
