import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../views/home.vue";
import MainWindow from "../views/mainwindow/mainwindow.vue";

const routesMainWindow = [
    {
        path: '/home',
        redirect: '/home/chat'
    },
    {
        path: 'chat',
        name: 'chat',
        component: MainWindow,
        meta: {
            title: ''
        },
        children: [
            {
                path: '/home/chat',
                redirect: '/home/chat/main'
            },
            {
                path: "main",
                name: "chat-main",
                icon: "el-icon-lx-home",
                meta: {
                    title: '聊天界面',
                    customTitle: true
                },
                component: () => import( /* webpackChunkName: "chat" */ "../views/mainwindow/chat/chat.vue")
            }
        ]
    },
    {
        path: 'calendar',
        name: 'calendar',
        component: MainWindow,
        meta: {
            title: ''
        },
        children: [
            {
                path: '/home/calendar',
                redirect: '/home/calendar/main'
            },
            {
                path: "main",
                name: "calendar-main",
                icon: "el-icon-lx-home",
                meta: {
                    title: '聊天界面',
                    customTitle: false
                },
                component: () => import( /* webpackChunkName: "calendar" */ "../views/mainwindow/calendar/calendar.vue")
            }
        ]
    },
    {
        path: 'organization',
        name: 'organization',
        component: MainWindow,
        meta: {
            title: ''
        },
        children: [
            {
                path: '/home/organization',
                redirect: '/home/organization/main'
            },
            {
                path: "main",
                name: "organization-main",
                icon: "el-icon-lx-home",
                meta: {
                    title: '聊天界面',
                    customTitle: true
                },
                component: () => import( /* webpackChunkName: "organization" */ "../views/mainwindow/organization/organization.vue")
            }
        ]
    },
    {
        path: 'workstation',
        name: 'workstation',
        component: MainWindow,
        meta: {
            title: ''
        },
        children: [
            {
                path: '/home/workstation',
                redirect: '/home/workstation/main'
            },
            {
                path: "main",
                name: "workstation-main",
                icon: "el-icon-lx-home",
                meta: {
                    title: '聊天界面',
                    customTitle: false
                },
                component: () => import( /* webpackChunkName: "workstation" */ "../views/mainwindow/workstation/workstation.vue")
            }
        ]
    },
    {
        path: 'mail',
        name: 'mail',
        component: MainWindow,
        meta: {
            title: ''
        },
        children: [
            {
                path: '/home/mail',
                redirect: '/home/mail/main/newmail'
            },
            {
                path: "main",
                name: "mail-main",
                icon: "el-icon-lx-home",
                meta: {
                    title: '聊天界面',
                    customTitle: false
                },
                component: () => import( /* webpackChunkName: "mail" */ "../views/mainwindow/mail/mailpane.vue"),
                children:[
                    {
                        path: "newmail",
                        name: "newmail",
                        meta: {
                            title: '新建邮件'
                        },
                        component: () => import ( /* webpackChunkName: "sendemail" */ "../views/mainwindow/mail/sendemail.vue")
                    }
                ]
            }
        ]
    },
    {
        path: 'meeting',
        name: 'meeting',
        component: MainWindow,
        meta: {
            title: ''
        },
        children: [
            {
                path: '/home/meeting',
                redirect: '/home/meeting/main'
            },
            {
                path: "main",
                name: "meeting-main",
                icon: "el-icon-lx-home",
                meta: {
                    title: '聊天界面',
                    customTitle: false
                },
                component: () => import( /* webpackChunkName: "meeting" */ "../views/mainwindow/meeting/meeting.vue")
            }
        ]
    },
    {
        path: 'diskinfo',
        name: 'diskinfo',
        component: MainWindow,
        meta: {
            title: ''
        },
        children: [
            {
                path: '/home/diskinfo',
                redirect: '/home/diskinfo/main'
            },
            {
                path: "main",
                name: "diskinfo-main",
                icon: "el-icon-lx-home",
                meta: {
                    title: '聊天界面',
                    customTitle: false
                },
                component: () => import( /* webpackChunkName: "diskinfo" */ "../views/mainwindow/diskinfo/diskinfo.vue")
            }
        ]
    },
    {
        path: 'about',
        name: 'about',
        component: MainWindow,
        meta: {
            title: ''
        },
        children: [
            {
                path: '/home/about',
                redirect: '/home/about/main'
            },
            {
                path: "main",
                name: "about-main",
                icon: "el-icon-lx-home",
                meta: {
                    title: '聊天界面',
                    customTitle: false
                },
                component: () => import( /* webpackChunkName: "about" */ "../views/mainwindow/about/about.vue")
            }
        ]
    }
];

const routes = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: "/home",
        name: "Home",
        component: Home,
        children: [...routesMainWindow]
    },
    {
        path: "/",
        name: "Login",
        component: Home,
        children: [
            {
                path: "/login",
                name: "login",
                icon: "el-icon-lx-home",
                meta: {
                    title: '登录'
                },
                component: () => import( /* webpackChunkName: "weather" */ "../views/login.vue")
            }
        ]
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

export default router;
